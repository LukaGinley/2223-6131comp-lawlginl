#include <Arduino.h>
#include <Sensor.h>
#include <RGB.h>
#include <Value.h>
#include <Rotary.h>

// Define the pins used by the system
// RGB LED pins
#define r_pin 4
#define g_pin 16
#define b_pin 17
// DHT11 temperature and humidity sensor pin
#define DHTPIN 15
#define DHTTYPE DHT11
// Rotary encoder pins
#define ROTARY_PIN_A 27
#define ROTARY_PIN_B 26
#define ROTARY_PIN_BUTTON 21
#define ROTARY_PIN_SWITCH 33

// Create an instance of the DHT11 sensor, RGB LED and Rotary encoder
DHT dht(DHTPIN, DHTTYPE);
// Testing: Values produce blue (HUMIDITY issue) and green (System OK) RGB outputs. 
Sensor sensor(DHTPIN, new RGB(r_pin, g_pin, b_pin), new Value(20.0, 30.0f), new Value(50, 80.0f));
// Testing: Values produce flashing red/blue (TEMPERATURE and HUMIDITY issue) and red (TEMPERATURE issue) RGB outputs. 
//Sensor sensor(DHTPIN, new RGB(r_pin, g_pin, b_pin), new Value(10.0, 15.0f), new Value(50, 80.0f));

Rotary rotary(ROTARY_PIN_A, ROTARY_PIN_B, ROTARY_PIN_BUTTON, ROTARY_PIN_SWITCH, sensor);

void setup() {
  // Initialize the serial port
  Serial.begin(115200);
  // Print a message to the console indicating that the DHT sensor is being calibrated
  Serial.println("Calibrating DHT Sensor...");
  // Wait for 2 seconds before starting to read the sensor
  delay(2000);
  // Begin reading the DHT11 sensor
  dht.begin();
}

void loop() {
  // Feature B: Log the temperature and humidity every 5 seconds
  static unsigned long lastLogMillis = 0;
  unsigned long currentMillis = millis();

  if (currentMillis - lastLogMillis >= 5000) {
    // Update lastLogMillis
    lastLogMillis = currentMillis;

    // Log the temperature and humidity readings from the sensor
    sensor.logTemperatureAndHumidity();
  }

  // Feature A: Handle temperature and humidity changes
  sensor.handleTemperatureAndHumidity();

  // Feature C: User input (Incomplete)
  //rotary.handle_encoder(sensor);
}