#ifndef VALUE_H
#define VALUE_H

class Value
{
public:
    // Constructor that takes a minimum and maximum value
    Value(float min_Value, float max_Value);
    
    // Returns the minimum value
    float getMinValue();
    
    // Returns the maximum value
    float getMaxValue();

    // Sets the minimum value
    void setMinValue(float min_Value);

    // Sets the minimum value
    void setMaxValue(float max_Value);

private:
    // Private variables to hold the minimum and maximum values
    float minimumValue;
    float maximumValue;
};

#endif