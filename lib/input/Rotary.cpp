#include "Rotary.h"
#include <ESP32Encoder.h>

// Constructor that initializes the member variables using an initialization list
Rotary::Rotary(int pinA, int pinB, int pinButton, int pinSwitch, Sensor &sensor)
    : pinA(pinA), pinB(pinB), pinButton(pinButton), pinSwitch(pinSwitch), currentMode(AdjustmentMode::MIN_TEMP), _sensor(sensor)
{
    encoder.attachHalfQuad(pinA, pinB);
    encoder.setCount(0);
}

// Setup function that sets the pinMode for the button and switch and attaches the encoder
void Rotary::setup()
{
    pinMode(pinButton, INPUT_PULLUP);
    pinMode(pinSwitch, INPUT_PULLUP);
    ESP32Encoder::useInternalWeakPullResistors = UP;
    encoder.attachHalfQuad(pinA, pinB);
    encoder.setCount(0);
}

// Loop function that checks for changes in the encoder count, button press, and switch state
void Rotary::loop()
{
    static int32_t lastEncoderCount = 0;  // Static variable to keep track of the last encoder count
    int32_t encoderCount = encoder.getCount();  // Get the current encoder count

    bool buttonPressed = digitalRead(pinButton) == LOW;  // Check if the button is pressed
    bool switchState = digitalRead(pinSwitch) == LOW;  // Check the state of the switch

    // If the button is pressed, switch the adjustment mode
    if (buttonPressed)
    {
        switchMode();
    }

    // If the encoder count has changed, adjust the min/max values
    if (lastEncoderCount != encoderCount)
    {
        adjustValues(encoderCount - lastEncoderCount);
        lastEncoderCount = encoderCount;
    }
}

// Function to switch the adjustment mode
void Rotary::switchMode()
{
    // Switch between the different adjustment modes
    switch (currentMode)
    {
    case AdjustmentMode::MIN_TEMP:
        currentMode = AdjustmentMode::MAX_TEMP;
        break;
    case AdjustmentMode::MAX_TEMP:
        currentMode = AdjustmentMode::MIN_HUMIDITY;
        break;
    case AdjustmentMode::MIN_HUMIDITY:
        currentMode = AdjustmentMode::MAX_HUMIDITY;
        break;
    case AdjustmentMode::MAX_HUMIDITY:
        currentMode = AdjustmentMode::MIN_TEMP;
        break;
    }
}

// Function to adjust the min/max values based on the current adjustment mode and encoder difference
void Rotary::adjustValues(int encoderDiff)
{
    float delta = encoderDiff * 0.5f;  // Calculate the delta value based on the encoder difference

    // Adjust the min/max values based on the current adjustment mode
    switch (currentMode)
    {
    case AdjustmentMode::MIN_TEMP:
        _sensor.setMinTemperature(_sensor.getMinTemperature() + delta);
        break;
    case AdjustmentMode::MAX_TEMP:
        _sensor.setMaxTemperature(_sensor.getMaxTemperature() + delta);
        break;
    case AdjustmentMode::MIN_HUMIDITY:
        _sensor.setMinHumidity(_sensor.getMinHumidity() + delta);
        break;
    case AdjustmentMode::MAX_HUMIDITY:
        _sensor.setMaxHumidity(_sensor.getMaxHumidity() + delta);
        break;
    }
}
