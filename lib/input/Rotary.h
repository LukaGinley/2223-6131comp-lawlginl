#ifndef ROTARY_H
#define ROTARY_H

#include <Arduino.h>
#include "Value.h"
#include "Sensor.h"
#include "ESP32Encoder.h"

class Rotary
{
public:
    // Constructor - initializes the Rotary object with the specified pin numbers and sensor object
    Rotary(int pinA, int pinB, int pinButton, int pinSwitch, Sensor &sensor);

    // Performs initial setup of the rotary encoder and pins
    void setup();

    // Continuously reads the rotary encoder and button states and adjusts values as necessary
    void loop();

private:
    // Pin numbers for rotary encoder and button/switch
    int pinA;
    int pinB;
    int pinButton;
    int pinSwitch;

    // Rotary encoder object
    ESP32Encoder encoder;

    // Sensor object used to adjust values
    Sensor &_sensor;

    // Enum to keep track of the current mode (minimum temperature, maximum temperature, minimum humidity, or maximum humidity)
    enum class AdjustmentMode
    {
        MIN_TEMP,
        MAX_TEMP,
        MIN_HUMIDITY,
        MAX_HUMIDITY
    } currentMode;

    // Switches to the next adjustment mode (e.g. from minimum temperature to maximum temperature)
    void switchMode();

    // Adjusts the corresponding sensor value (temperature or humidity) based on the rotary encoder input
    void adjustValues(int encoderDiff);
};

#endif

