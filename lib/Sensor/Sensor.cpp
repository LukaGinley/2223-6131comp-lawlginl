#include "Sensor.h"
#include <DHT.h>

// Constructor: Initializes the Sensor object with the given parameters and sets initial values
Sensor::Sensor(int pin, RGB *rgb, Value *tempValue, Value *humValue)
    : _pin(pin),
      dhtSensor(pin, DHT11),
      rgbLED(rgb),
      temperatureRange(tempValue),
      humidityRange(humValue),
      temperature(0),
      humidity(0),
      lastFlashMillis(0),
      redLightOn(false)
{
    dhtSensor.begin(); // Start DHT sensor
}

// Returns the last registered temperature
float Sensor::readTemperature()
{
    return temperature;
}

// Returns the last registered humidity
float Sensor::readHumidity()
{
    return humidity;
}

// Checks the temperature and humidity readings and changes the RGB LED accordingly
void Sensor::handleTemperatureAndHumidity()
{
    // Read temperature and humidity
    temperature = dhtSensor.readTemperature();
    humidity = dhtSensor.readHumidity();

    // Check if temperature and/or humidity readings are out of range
    boolean isTemperatureOutOfRange = temperature < temperatureRange->getMinValue() || temperature > temperatureRange->getMaxValue();
    boolean isHumidityOutOfRange = humidity < humidityRange->getMinValue() || humidity > humidityRange->getMaxValue();

    // Handle out of range readings
    if (isTemperatureOutOfRange && isHumidityOutOfRange)
    {
        // Flash the LED between red and blue
        unsigned long currentMillis = millis();
        if (currentMillis - lastFlashMillis >= 500)
        {
            lastFlashMillis = currentMillis;
            if (redLightOn)
            {
                rgbLED->setColour(0, 0, 255); // Blue
                redLightOn = false;
            }
            else
            {
                rgbLED->setColour(255, 0, 0); // Red
                redLightOn = true;
            }
        }
    }
    else if (isTemperatureOutOfRange)
    {
        rgbLED->setColour(255, 0, 0); // Red
    }
    else if (isHumidityOutOfRange)
    {
        rgbLED->setColour(0, 0, 255); // Blue
    }
    else
    {
        // System OK, set LED to green
        rgbLED->setColour(0, 255, 0); // Green
    }
}

// Logs the sensor readings and specified min/max values to the Serial Monitor
void Sensor::logTemperatureAndHumidity()
{
    // Print current temperature and humidity readings
    Serial.println("- Current Readings -");
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.println("C");
    Serial.print("Humidity: ");
    Serial.print(humidity);
    Serial.println("%");

    // Check if temperature and/or humidity readings are out of range
    boolean isTemperatureOutOfRange = temperature < temperatureRange->getMinValue() || temperature > temperatureRange->getMaxValue();
    boolean isHumidityOutOfRange = humidity < humidityRange->getMinValue() || humidity > humidityRange->getMaxValue();

    // Print specified min/max values
    Serial.println("--------------------------------");
    Serial.println("| Temperature  | MIN   | MAX   |");
    Serial.println("|    Value     |-------|-------|");
    Serial.print("|              | ");
    Serial.print(temperatureRange->getMinValue(), 2);
    Serial.print(" | ");
    Serial.print(temperatureRange->getMaxValue(), 2);
    Serial.println(" |");
    Serial.println("|--------------|-------|-------|");
    Serial.println("| Humidity     | MIN   | MAX   |");
    Serial.print("|    Value     | ");
    Serial.print(humidityRange->getMinValue(), 2);
    Serial.print(" | ");
    Serial.print(humidityRange->getMaxValue(), 2);
    Serial.println(" |");
    Serial.println("--------------------------------");

    // Check if temperature and/or humidity is out of range and print appropriate message
    if (isTemperatureOutOfRange && isHumidityOutOfRange)
    {
        Serial.println("\nTEMPERATURE and HUMIDITY issue.\n");
    }
    else if (isTemperatureOutOfRange)
    {
        Serial.println("\nTEMPERATURE issue.\n");
    }
    else if (isHumidityOutOfRange)
    {
        Serial.println("\nHUMIDITY issue.\n");
    }
    else
    {
        Serial.println("\nSystem OK.\n");
    }
}