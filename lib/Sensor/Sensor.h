#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include <DHT.h>
#include "RGB.h"
#include "Value.h"

class Sensor
{
public:
    // Constructor that takes the pin for the DHT sensor, an RGB object, and two Value objects for temperature and humidity ranges
    Sensor(int pin, RGB *rgb, Value *tempValue, Value *humValue);
    
    // Returns the current temperature
    float readTemperature();
    
    // Returns the current humidity
    float readHumidity();

    // Handles changes in temperature and humidity and updates the RGB LED
    void handleTemperatureAndHumidity();
    
    // Logs the current temperature and humidity readings to the Serial monitor
    void logTemperatureAndHumidity();

private:
    // Private variables to hold the pin, DHT object, RGB object, and temperature and humidity ranges
    int _pin;
    DHT dhtSensor;
    RGB *rgbLED;
    Value *temperatureRange;
    Value *humidityRange;
    
    // Private variables to hold the current temperature and humidity readings, as well as information for flashing the RGB LED
    float temperature;
    float humidity;
    unsigned long lastFlashMillis;
    bool redLightOn;
    int flashCounter;
};

#endif // SENSOR_H
