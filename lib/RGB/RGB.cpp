#include "RGB.h"

// Constructor: initializes RGB pins as OUTPUT
RGB::RGB(int red_pin, int green_pin, int blue_pin)
{
    r_pin = red_pin;
    g_pin = green_pin;
    b_pin = blue_pin;

    pinMode(red_pin, OUTPUT);    // Set red pin as OUTPUT
    pinMode(green_pin, OUTPUT);    // Set green pin as OUTPUT
    pinMode(blue_pin, OUTPUT);    // Set blue pin as OUTPUT
}

// Function: sets RGB color
void RGB::setColour(int red, int green, int blue)
{
    analogWrite(r_pin, red);  // Write red value to red pin
    analogWrite(g_pin, green);  // Write green value to green pin
    analogWrite(b_pin, blue);  // Write blue value to blue pin
}